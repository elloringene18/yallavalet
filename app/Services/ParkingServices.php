<?php namespace App\Services;

use App\Models\Cars\Car;
use App\Models\Parking\ParkedCarRequest;
use App\Models\Parking\ParkingSpot;
use App\Models\Users\User;
use App\Models\Establishments\Branch;
use App\Models\Parking\ParkedCar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Services\PusherServices;

class ParkingServices
{

    public function __construct(ParkedCar $model, User $users, Branch $branch, ParkedCar $parkedCar, Car $car, ParkingSpot $parkingSpot, ParkedCarRequest $carRequest, PusherServices $pusher)
    {
        $this->model      = $model;
        $this->user       = $users;
        $this->branch     = $branch;
        $this->parking    = $parkingSpot;
        $this->parked     = $parkedCar;
        $this->carRequest = $carRequest;
        $this->pusher     = $pusher;
        $this->car        = $car;
    }

    public function update()
    {

    }

    public function forUser($request)
    {

        $user = User::find($request->input('id'));

        if ($user != null) {
            $newRow = $user->parkedCars->create($request->except('token', 'id'));

            if ($newRow != null)
                return $newRow;

            return false;
        }
    }

    public function fromBranch($request)
    {

        $user = $this->branch->find($request->input('id'));

        if ($user != null) {
            $newRow = $user->parkedCars->create($request->except('token', 'id'));

            if ($newRow != null)
                return $newRow;

            return false;
        }
    }

    public function park($request)
    {

        $data = $request->input();

        if ($this->parking->find($data['parking_spot_id']) == null)
            return ['success' => false, 'error' => 'parking-spot-404'];

        if ($this->car->find($data['car_id']) == null)
            return ['success' => false, 'error' => 'car-404'];

        $dataToStore              = $request->except('token', 'id');
        $dataToStore['parked_on'] = Carbon::now();

        if (ParkedCar::where([['car_id', $data['car_id']], ['returned_on', null]])->count())
            return ['success' => false, 'error' => 'car-already-parked'];

        if (ParkedCar::where([['parking_spot_id', $data['parking_spot_id']], ['returned_on', null]])->count())
            return ['success' => false, 'error' => 'parking-spot-taken'];

        $newRow = $this->parked->create($dataToStore);

        if ($newRow != null){
            $newRow->load('parking_spot.branch');

            $channel = $newRow->parking_spot->branch->pusher_channel;
            $this->pusher->push($channel,'car-parked');

            $newRow['channel'] = $channel;

            return ['success' => true, 'data' => $newRow];
        }

        return ['success' => false, 'error' => 'error'];
    }

    public function requestACar($request)
    {
        $data   = $request->input();
        $target = $this->parked->find($data['parked_car_id']);

        if ($target == null)
            return ['success' => false, 'error' => 'car-404'];

        $request = ParkedCarRequest::where('parked_car_id', $target->id)->first();

        if ($request == null) {
            $newRow = $target->request()->create([
                'parked_car_id'
            ]);
        }

        if ($newRow != null)
            return ['success' => true, 'data' => $newRow];

        return ['success' => false, 'error' => 'error'];
    }

    public function carRequestAccepted($request)
    {
        $driver = Auth::user();
        $data   = $request->input();

        $target = $this->carRequest->find($data['parked_car_request_id']);

        if ($target == null)
            return ['success' => false, 'error' => 'error-404'];

        if ($target->update(['driver_id' => $driver->id, 'status' => 1]))
            return ['success' => true, 'data' => $target->load('driver')];

        return ['success' => false, 'error' => 'error'];
    }

    public function carReturned($request)
    {
        $data   = $request->input();
        $target = $this->carRequest->find($data['parked_car_request_id']);

        if ($target == null)
            return ['success' => false, 'error' => 'car-404'];

        $target->update(['status' => 3]);
        $target->parked_car->update(['returned_on' => Carbon::now()]);

        return ['success' => true, 'data' => $target->load('driver')];
    }

    public function carRequestCancelled($request) {
        $data   = $request->input();
        $target = $this->carRequest->find($data['parked_car_request_id']);

        if ($target == null)
            return ['success' => false, 'error' => 'car-404'];

        $target->update(['driver_id' => null, 'status' => 0]);

        return ['success' => true, 'data' => $target->load('driver')];
    }

    public function carArrived($request) {
        $data   = $request->input();
        $target = $this->carRequest->find($data['parked_car_request_id']);

        if ($target == null)
            return ['success' => false, 'error' => 'car-404'];

        $target->update(['driver_id' => null, 'status' => 2]);

        return ['success' => true, 'data' => $target->load('driver')];
    }


    public function getAvailableFromBranch($branch_id)
    {

        $unavailableSpots = ParkingSpot::whereHas('parkedCars', function ($q) {
            $q->where('returned_on', null);
        })->where('branch_id', $branch_id)->select('id')->get()->toArray();

        $availableSpots = ParkingSpot::where('branch_id', $branch_id)
                                     ->whereNotIn('id', $unavailableSpots)
                                     ->orderBy('code')
                                     ->get();

        $data['available']   = $availableSpots;
        $data['unavailable'] = count($unavailableSpots);

        return $data;
    }

    public function getParkedFromBranch($branch_id)
    {
        return ParkedCar::with('car', 'car.users', 'request', 'request.driver', 'parking_spot')
            ->whereHas('parking_spot.branch', function ($q) use ($branch_id) {
                $q->where('branch_id', $branch_id);
            })->where('returned_on', null)->get();
    }

    public function getParkedCarsForUser($user_id){
        return ParkedCar::whereHas('car.users', function($q) use($user_id){
            $q->where('user_id', $user_id);
        })->where('returned_on', null)->get();
    }

    public function getParkedCarRequestsFromBranch($branch_id)
    {
        return ParkedCarRequest::with('driver')
                               ->whereHas('parked_car.parking_spot.branch', function ($q) use ($branch_id) {
                                   $q->where('branch_id', $branch_id);
                               })->where('status', 0)->get();
    }

    public function getParkedFromUser($user_id)
    {
        return ParkedCar::with([
            'car', 'car.users', 'request', 'parking_spot', 'parking_spot.branch', 'parking_spot.branch.establishment'
        ])->whereHas('car.users', function ($q) use ($user_id) {
            $q->where('user_id', $user_id);
        })->where('returned_on', null)->get();
    }
}