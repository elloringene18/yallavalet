<?php namespace App\Services;

use Illuminate\Support\Str;

trait CanCRUD {

    public function deleteById($id)
    {
        $record = $this->model->find($id);

        if(!$record)
            return $this->generateResponse('not-found');

        if($record->delete())
            return $this->generateResponse('delete-success');

        return $this->generateResponse('error');
    }

    public function addRecord($request,$checkName){

        if($checkName){
            $existing = $this->model->where('name',$request->input('name'))->count();

            if($existing)
                return $this->generateResponse('name-exists');
        }

        $newRow = $this->model->create($request->except('token'));

        if($newRow != false)
            return $this->generateResponseWithData('create-success',$newRow);

        return $this->generateResponse('error');
    }
}