<?php namespace App\Services;

use App\Models\Users\User;
use Codecasts\Auth\JWT\Auth\Guard;
use Illuminate\Support\Facades\Auth;

trait CanJWT
{
    /**
     * Generate API token for a given user
     *
     * @param User  $user
     * @param array $claims
     *
     * @return bool|string
     */
    public function generateTokenForUser(User $user, $claims = [])
    {
        $auth = $this->guard();

        $auth->login($user);

        $token = $auth->issue($claims);

        return $token;
    }

    /**
     * Refresh API Token
     *
     * @return bool|string
     */
    public function refreshToken()
    {
        $auth = $this->guard();

        $token = $auth->refresh();

        return $token;
    }

    /**
     * Return Guard instance
     *
     * @return Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}