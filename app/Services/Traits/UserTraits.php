<?php namespace App\Services\Traits;

trait UserTraits {

    public function scopeGetFromUser($query,$user_id)
    {
        return $query->whereHas('users',function($querySub) use($user_id){
            $querySub->where('user_id',$user_id);
        });
    }
}

