<?php namespace App\Services;

use App\Models\Cars\Car;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CarServices
{
    public function getFromUser($user_id)
    {
        return Car::whereHas('users', function ($query) use ($user_id) {
            $query->where('user_id', $user_id);
        })->get();
    }

    public function store($request)
    {

        $user = Auth::user();

        if ($user == null)
            return ['success' => false, 'error' => 'user-404'];

        if (Car::where('plate_number', $request->input('plate_number'))->first())
            return ['success' => false, 'error' => 'car-exists'];

        $car = $user->cars()->create($request->except('user_id', 'token', 'plate_code_id'));

        if ($car != null) {
			
            $car->plateCode()->sync($request->input('plate_code_id'));

            // TODO: Change $car_id to a code e.g. XA17-2472-0002
			$qrCode = base64_encode(QrCode::format('png')->size(300)->generate($car->id));
			
			$car->qrCode()->create(['base64_code'=>$qrCode]);
		
            $car->load('plateCode');

            return ['success' => true, 'data' => $car];
        }

        return ['success' => false, 'error' => 'error'];
    }

    public function update(Car $car, $request)
    {
        $user = Auth::user();

        if ($user == null)
            return ['success' => false, 'error' => 'user-404'];

        $target = $car;

        if ($target == null)
            return ['success' => false, 'error' => 'car-404'];

        if ($target->plate_number == $request->input('plate_number') && $target->id != $car->id)
            return ['success' => false, 'error' => 'car-exists'];

        $target->update($request->except('user_id', 'token', 'plate_code_id'));

        $target->plateCode()->sync($request->input('plate_code_id'));
        $target->load('plateCode');

        return ['success' => true, 'data' => $target];

    }

}