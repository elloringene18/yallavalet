<?php namespace App\Services;

use Illuminate\Support\Str;

trait CanCreateResponseCode
{

    public function generateResponseWithData($slug, $postData)
    {

        $data         = $this->make($slug);
        $data['data'] = $postData;

        return $data;
    }

    public function generateResponseWithError($slug, $error = [])
    {
        $data          = $this->make($slug);
        $data['error'] = $error;

        return $data;
    }

    public function generateResponseWithParameter($slug, $param)
    {

        switch ($slug) {
            case 'work-hour-exceeded-dept':
                $data['status']  = 500;
                $data['message'] = 'Time entered exceeds working hours of ' . $param . ' hour(s).';
                break;
        }

        return $data;
    }

    public function generateResponse($slug)
    {
        $data = $this->make($slug);

        return $data;
    }

    private function make($slug)
    {
        $data['success'] = true;
        $data['status']  = 200;

        switch ($slug) {
            case 'create-success':
                $data['message'] = 'New record created successfully.';
                break;

            case 'invite-success':
                $data['message'] = 'Your message has been sent.';
                break;

            case 'update-success':
                $data['message'] = 'Record updated successfully.';
                break;

            case 'invite-sent-success':
                $data['message'] = 'An invitation has been sent to ';
                break;

            case 'delete-success':
                $data['message'] = 'Record deleted successfully.';
                break;

            case 'token-found':
                $data['message'] = 'Token validated. Returning data.';
                break;

            case 'record-found':
                $data['message'] = 'Record retrieved successfully';
                break;

            case 'success':
                $data['message'] = 'Operation was successful.';
                break;

            // MOBILE API

            case 'unauthorized':
                $data['success'] = false;
                $data['status']  = 401;
                $data['message'] = 'Unauthorized.';
                break;

            case 'invalid-credentials':
                $data['success'] = false;
                $data['status']  = 401;
                $data['message'] = 'Invalid email/password combination.';
                break;

            case 'invalid-api-token':
                $data['success'] = false;
                $data['status']  = 401;
                $data['message'] = 'Invalid token.';
                break;

            case 'existing-token':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'Token already exists in database.';
                break;

            case 'name-exists':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'The name provided is already in use.';
                break;

            case 'not-found':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'Record was not found.';
                break;

            case 'parking-spot-404':
                $data['success'] = false;
                $data['status']  = 404;
                $data['message'] = 'Parking spot not found.';
                break;

            case 'car-404':
                $data['success'] = false;
                $data['status']  = 404;
                $data['message'] = 'Car not found.';
                break;

            case 'car-exists':
                $data['success'] = false;
                $data['status']  = 404;
                $data['message'] = 'Plate number already exists.';
                break;

            case 'user-404':
                $data['success'] = false;
                $data['status']  = 404;
                $data['message'] = 'User not found.';
                break;

            case 'no-record-found':
                $data['success'] = false;
                $data['status']  = 404;
                $data['message'] = 'Request return empty.';
                break;

            case 'validation-error':
                $data['success'] = false;
                $data['status']  = 422;
                $data['message'] = 'Invalid inputs.';
                break;

            case 'error':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'There was an unexpected error.';
                break;

            case 'car-already-parked':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'This car is already parked in the system.';
                break;

            case 'parking-spot-taken':
                $data['success'] = false;
                $data['status']  = 500;
                $data['message'] = 'This parking spot is already taken in the system.';
                break;


        }

        return $data;
    }
}