<?php namespace App\Services;

use App\Models\Cars\Car;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PusherServices
{
    public function __construct()
    {
        $this->pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            array(
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'encrypted' => false
            )
        );
    }

    public function push($channel,$event, $data = null){
        return $this->pusher->trigger($channel, $event, $data,null, true);
    }

}