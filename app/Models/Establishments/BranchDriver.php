<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class BranchDriver extends Model
{
    public $timestamps = false;
}
