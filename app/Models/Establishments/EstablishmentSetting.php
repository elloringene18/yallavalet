<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class EstablishmentSetting extends Model
{
    protected $fillable = ['establishment_id','key','value'];

    public $hidden = ['created_at','updated_at','establishment_id'];
}
