<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class BranchSetting extends Model
{
    protected $fillable = ['branch_id','key','value'];

    public $hidden = ['created_at','updated_at','branch_id'];
}
