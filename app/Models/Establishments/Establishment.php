<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    protected $fillable = ['name', 'establishment_type_id'];
    protected $hidden   = ['created_at', 'updated_at'];

    public function type()
    {
        return $this->belongsTo('App\Models\Establishments\EstablishmentType', 'establishment_type_id');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\Establishments\Branch');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Establishments\EstablishmentSetting');
    }

}
