<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['establishment_id','name','pusher_channel'];
    protected $hidden = ['created_at','updated_at'];

    public function establishment()
    {
        return $this->belongsTo('App\Models\Establishments\Establishment');
    }

    public function settings(){
        return $this->hasMany('App\Models\Establishments\BranchSetting');
    }

    public function parkingSpots(){
        return $this->hasMany('App\Models\Parking\ParkingSpot');
    }

    public function drivers(){
        return $this->belongsToMany('App\Models\Users\User','branch_drivers');
    }
}
