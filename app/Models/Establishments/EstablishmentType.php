<?php

namespace App\Models\Establishments;

use Illuminate\Database\Eloquent\Model;

class EstablishmentType extends Model
{
    protected $fillable = ['name','slug'];

    public $timestamps = false;
}
