<?php

namespace App\Models\PaymentMethods;

use App\Services\Traits\UserTraits;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use UserTraits;

    protected $fillable = ['card_type','card_number','card_expire','card_cvv','card_label'];

    public function users(){
        return $this->belongsToMany('App\Models\Users\User','user_payment_methods');
    }
}
