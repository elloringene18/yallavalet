<?php

namespace App\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function cars(){
        return $this->belongsToMany('App\Models\Cars\Car','user_cars');
    }

    public function role(){
        return $this->belongsToMany('App\Models\Users\Role','user_roles');
    }

    public function branch()
    {
        return $this->belongsToMany('App\Models\Establishments\Branch', 'branch_drivers')->withPivot('id');
    }

    public function paymentMethods(){
        return $this->belongsToMany('App\Models\PaymentMethods\PaymentMethod','user_payment_methods');
    }

    public function getRoleAttribute(){
        return $this->role()->first();
    }

    public function scopeWithCars($query)
    {
        return $query->with('cars');
    }
}
