<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserCar extends Model
{
    public function user(){
        return $this->hasOne('App\Models\Users\User');
    }
    public function car(){
        return $this->hasOne('App\Models\Cars\Car');
    }
}
