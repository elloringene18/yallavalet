<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserPaymentMethod extends Model
{
    protected $fillable = ['user_id','payment_method_id'];
}
