<?php namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ["name","slug"];
	
	public $timestamps = false;
}
