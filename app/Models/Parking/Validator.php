<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validator extends Model
{
    protected $fillable = ['name','location','pin','branch_id'];

}
