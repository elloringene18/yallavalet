<?php

namespace App\Models\Parking;

use Illuminate\Database\Eloquent\Model;

class ParkingSpot extends Model
{
    protected $fillable = ['branch_id','code','price'];

    public function parkedCars(){
        return $this->belongsToMany('App\Models\Cars\Car','parked_cars');
    }

    public function branch(){
        return $this->belongsTo('App\Models\Establishments\Branch');
    }
}
