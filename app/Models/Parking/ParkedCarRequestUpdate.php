<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkedCarRequestUpdate extends Model
{
    protected $fillable = ['parked_car_request_id','driver_id','parked_car_request_type_id'];


    public function type()
    {
        return $this->hasOne('App\Models\Parking\ParkedCarRequestType');
    }

    public function driver()
    {
        return $this->hasOne('App\Models\Establishment\BranchDriver');
    }

    public function request()
    {
        return $this->hasOne('App\Models\Parking\ParkedCarRequest');
    }
}
