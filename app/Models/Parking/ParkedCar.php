<?php

namespace App\Models\Parking;

use Illuminate\Database\Eloquent\Model;

class ParkedCar extends Model
{
    protected $fillable = ['parking_spot_id','parked_on','returned_on','car_id'];

    public function car(){
        return $this->belongsTo('App\Models\Cars\Car');
    }

    public function request(){
        return $this->hasOne('App\Models\Parking\ParkedCarRequest');
    }

    public function parking_spot(){
        return $this->belongsTo('App\Models\Parking\ParkingSpot');
    }


}
