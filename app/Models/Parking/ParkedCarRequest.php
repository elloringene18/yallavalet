<?php

namespace App\Models\Parking;

use Illuminate\Database\Eloquent\Model;

class ParkedCarRequest extends Model
{
    //  'parked_car_id, branch_driver_id, status
    protected $fillable = ['parked_car_id','driver_id','status'];

    public function parked_car(){
        return $this->belongsTo('App\Models\Parking\ParkedCar','parked_car_id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Models\Users\User', 'driver_id');
    }

    public function updates()
    {
        return $this->hasMany('App\Models\Parking\ParkedCarRequestUpdate');
    }
}
