<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValidatorQrCode extends Model
{
    protected $fillable = ['base64_code','validator_id'];

    public $timestamps = false;

    public function validator(){
        return $this->hasOne('App\Models\Parking\Validator');
    }
}
