<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ["name","code"];
    public $timestamps = false;
	
	

    public function plateCode(){
        return $this->hasMany('App\Models\Cars\PlateCode');
    }

}
