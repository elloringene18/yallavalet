<?php

namespace App\Models\Cars;

use App\Services\Traits\UserTraits;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use UserTraits;

    protected $fillable = ['name', 'plate_number','car_make_id','car_model_id','car_color_id','car_type_id','plate_type_id'];
    protected $hidden = ['car_make_id','car_model_id','car_color_id','car_type_id','plate_type_id'];

    public function models(){
        return $this->hasMany('App\Models\CarModel');
    }

    public function users(){
        return $this->belongsToMany('App\Models\Users\User','user_cars');
    }

    public function type(){
        return $this->belongsTo('App\Models\Cars\CarType','car_type_id');
    }

    public function plateType(){
        return $this->belongsTo('App\Models\Cars\PlateType','plate_type_id');
    }

    public function qrCode(){
        return $this->hasOne('App\Models\Cars\CarQrCode','car_id');
    }

    public function make(){
        return $this->belongsTo('App\Models\Cars\CarMake', 'car_make_id');
    }

    public function model(){
        return $this->belongsTo('App\Models\Cars\CarModel', 'car_model_id');
    }

    public function color(){
        return $this->belongsTo('App\Models\Cars\CarColor', 'car_color_id');
    }

    public function plateCode(){
        return $this->belongsToMany('App\Models\Cars\PlateCode','car_plate_codes');
    }

    public function parked(){
        return $this->hasMany('App\Models\Establishments\ParkedCar');
    }

    public function scopeWithTraits($query)
    {
        return $query->with('type','color','make','model','plateType','plateCode','qrCode');
    }

    public function getFormattedPlateNumberAttribute() {
        $code = $this->plateCode()->first();

        return $code ? "{$code->code} {$this->plate_number}" : $this->plate_number;
    }

}
