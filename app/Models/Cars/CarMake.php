<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class CarMake extends Model
{
    protected $fillable = ["name"];

    public $timestamps = false;

    public function models(){
        return $this->hasMany('App\Models\Cars\CarModel');
    }
}
