<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class CarQrCode extends Model
{
    protected $fillable = ['base64_code'];
	
	public $timestamps = false;
}
