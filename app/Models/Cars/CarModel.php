<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    protected $fillable = ['name','car_make_id'];

    public $timestamps = false;
}
