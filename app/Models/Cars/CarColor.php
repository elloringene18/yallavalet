<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class CarColor extends Model
{
    protected $fillable = ["name"];
    public $timestamps = false;
}
