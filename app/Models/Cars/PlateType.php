<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class PlateType extends Model
{
    protected $fillable = ["name"];
    public $timestamps = false;
}
