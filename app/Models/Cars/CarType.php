<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $fillable = ["name"];
    public $timestamps = false;
}
