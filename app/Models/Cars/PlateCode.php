<?php

namespace App\Models\Cars;

use Illuminate\Database\Eloquent\Model;

class PlateCode extends Model
{
    protected $fillable   = ["code", "country_code"];
    public    $timestamps = false;

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
