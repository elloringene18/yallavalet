<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\Establishments\Branch;
use App\Models\Establishments\BranchSetting;
use App\Models\Parking\ParkingSpot;
use App\Services\CanCRUD;
use Illuminate\Http\Request;


class BranchController extends ApiController
{
    use CanCRUD;

    public function __construct(Branch $model, BranchSetting $branchSetting)
    {
        $this->model = $model;
        $this->branches = $branchSetting;
    }

    public function all()
    {
        return $this->model->get();
    }

    public function store(Request $request)
    {
        return response()->json($this->addRecord($request,true));
    }

    public function delete(Request $request, $id)
    {
        return response()->json($this->deleteById($id));
    }

    /**
     ************************************************
     * SETTINGS
     ************************************************
     **/

    public function getSettings($id)
    {
        $record = $this->model->find($id);

        if (!$record)
            return response()->json($this->generateResponse('not-found'));

        $data = BranchSetting::where('branch_id', $id)->get();

        return response()->json($this->generateResponseWithData('record-found', $data));
    }

    public function addSettings(Request $request)
    {
        $record = $this->model->find($request->input('branch_id'));

        if(!$record)
            return $this->generateResponse('not-found');

        $data = $request->input('data');

        $newData = [];
        foreach ($data as $datum)
        {
            $newData[] = $record->settings()->create($datum);
        }

        return $this->generateResponseWithData('create-success',$newData);
    }

    public function deleteSettings(Request $request, $id)
    {
        $record = BranchSetting::find($id);

        if(!$record)
            return $this->generateResponse('not-found');

        if($record->delete())
            return $this->generateResponse('delete-success');

        return $this->generateResponse('error');
    }
    /**
     ************************************************
     * PARKING SPOTS
     ************************************************
     **/

    public function getParking($id)
    {
        $record = $this->model->find($id);

        if (!$record)
            return response()->json($this->generateResponse('not-found'));

        $data = ParkingSpot::where('branch_id', $id)->get();

        return response()->json($this->generateResponseWithData('record-found', $data));
    }

    public function addParking(Request $request)
    {
        $record = $this->model->find($request->input('branch_id'));

        if(!$record)
            return $this->generateResponse('not-found');

        $data = $request->input('data');

        $newData = [];
        foreach ($data as $datum)
        {
            $newData[] = $record->parkingSpots()->create($datum);
        }

        return $this->generateResponseWithData('create-success',$newData);
    }

    public function deleteParking(Request $request, $id)
    {
        $record = ParkingSpot::find($id);

        if(!$record)
            return $this->generateResponse('not-found');

        if($record->delete())
            return $this->generateResponse('delete-success');

        return $this->generateResponse('error');
    }

}
