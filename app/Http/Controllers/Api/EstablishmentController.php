<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\Establishments\Establishment;
use App\Models\Establishments\EstablishmentSetting;
use App\Models\Establishments\EstablishmentType;
use App\Services\CanCRUD;
use App\Services\EstablishmentServices;
use Illuminate\Http\Request;

class EstablishmentController extends ApiController
{
    use CanCRUD;

    public function __construct(Establishment $model, EstablishmentType $establishmentType, EstablishmentServices $establishmentServices)
    {
        $this->model = $model;
        $this->type = $establishmentType;
        $this->establishment = $establishmentServices;
    }

    public function all()
    {
        return $this->model->with('branches.settings','settings')->get();
    }

    public function store(Request $request)
    {
        return response()->json($this->addRecord($request,true));
    }

    public function delete(Request $request, $id)
    {
        return response()->json($this->deleteById($id));
    }

    /**
     ************************************************
     * SETTINGS
     ************************************************
     **/

    public function getSettings($id)
    {
        $establishment = $this->model->find($id);

        if (!$establishment)
            return response()->json($this->generateResponse('not-found'));

        $data = EstablishmentSetting::where('establishment_id', $id)->get();

        return response()->json($this->generateResponseWithData('record-found', $data));
    }

    public function addSettings(Request $request)
    {
        $establishment = $this->model->find($request->input('establishment_id'));

        if(!$establishment)
            return $this->generateResponse('not-found');

        $data = $request->input('data');

        $newData = [];
        foreach ($data as $datum)
        {
            $newData[] = $establishment->settings()->create($datum);
        }

        return $this->generateResponseWithData('create-success',$newData);
    }

    public function deleteSettings(Request $request, $id)
    {
        $record = EstablishmentSetting::find($id);

        if(!$record)
            return $this->generateResponse('not-found');

        if($record->delete())
            return $this->generateResponse('delete-success');

        return $this->generateResponse('error');
    }

}
