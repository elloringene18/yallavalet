<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Models\Users\Role;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends ApiController
{
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)) {
            $auth          = Auth::user();
            $user          = User::with('role')->find($auth->id);
            $user_resource = new UserResource($user);
            $user_resource->addToken($this->generateTokenForUser($user));

            return response()->json($this->generateResponseWithData('success', $user_resource));
        }

        return response()->json($this->generateResponse('invalid-credentials'));
    }

    public function facebook()
    {

    }

    public function google()
    {

    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|email|max:255|unique:users',
            'password'   => 'required|min:6|confirmed'
        ]);

        if ($validator->passes()) {
            /** @var User $user */
            $user = User::create([
                'name'     => $request->get('first_name') . ' ' . $request->get('last_name'),
                'email'    => $request->get('email'),
                'password' => bcrypt($request->get('password')),
            ]);

            $user_role = Role::where('slug', 'user')->first();
            $user->role()->attach($user_role->id);

            // TODO: Add Event and send verification email here
            $response            = $this->generateResponse('create-success');
            $response['message'] = "Successfully registered.";

            return response()->json($response);
        }

        $errors = $validator->errors()->toArray();

        return response()->json($this->generateResponseWithError('validation-error', $errors));
    }
}