<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CanCreateResponseCode;
use App\Services\CanJWT;

class ApiController extends Controller {
    use CanJWT, CanCreateResponseCode;
}