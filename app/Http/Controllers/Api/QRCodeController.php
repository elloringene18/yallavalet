<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CarResource;
use App\Models\Cars\Car;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeController extends ApiController
{
    /**
     * Find car with the supplied $id
     *
     * @param $id
     *
     * @return mixed
     */
    public function findCarById($id)
    {
        $car = Car::find($id);

        if (!is_null($car)) {
            $user = $car->users()->first();

            $result = [
                'car'  => new CarResource($car),
                'user' => [
                    'name'  => $user->name,
                    'email' => $user->email,
                ]
            ];

            return $this->generateResponseWithData('success', $result);
        }

        return $this->generateResponse('car-404');
    }

    public function qr()
    {
        $string = base64_encode(QrCode::format('png')->size(500)->generate('https://www.simplesoftware.io/docs/simple-qrcode'));

        echo "<img src='data:image/png;base64, " . $string . "'>";
    }

    function base64tojpeg($base64_string, $output_file)
    {
        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64_string);

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));

        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }
}

