<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\CarColorResource;
use App\Http\Resources\CarMakeResource;
use App\Http\Resources\CarModelResource;
use App\Http\Resources\CarResource;
use App\Http\Resources\CarTypeResource;
use App\Http\Resources\PlateCodeResource;
use App\Http\Resources\PlateTypeResource;
use App\Http\Resources\UserResource;
use App\Models\Cars\CarColor;
use App\Models\Cars\CarMake;
use App\Models\Cars\CarModel;
use App\Models\Cars\CarPlateCode;
use App\Models\Cars\CarType;
use App\Models\Cars\PlateCode;
use App\Models\Cars\PlateType;
use App\Models\Users\User;
use App\Services\CanCRUD;
use Illuminate\Http\Request;
use App\Models\Cars\Car;

use App\Services\CarServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CarController extends ApiController
{
    use CanCRUD;

    public function __construct(Car $car, CarServices $carServices)
    {
        $this->model = $car;
        $this->cars  = $carServices;
    }

    public function fromCurrentUser()
    {
        $user = Auth::user();
        $cars = $user->cars()->withTraits()->get();

        return $this->generateResponseWithData('success', CarResource::collection($cars));
    }

    public function fromUser($id)
    {
        $user = User::find($id);

        if ($user == null)
            return $this->generateResponse('user-404');

        $cars = $this->model->getFromUser($user->id)->withTraits()->get();

        return $this->generateResponseWithData('success', $cars);
    }

    public function all()
    {
        return $this->model->withTraits()->get();
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        // TODO: validate passed ids
        $validator = Validator::make($request->all(), [
            'name'          => 'bail|required|string',
            'plate_number'  => 'bail|required|string|max:5',
            'car_make_id'   => 'bail|required|integer',
            'car_model_id'  => 'bail|required|integer',
            'car_color_id'  => 'bail|required|integer',
            'car_type_id'   => 'bail|required|integer',
            'plate_type_id' => 'bail|required|integer',
            'plate_code_id' => 'bail|nullable|integer',
        ]);

        if ($validator->passes()) {
            $newRow = $this->cars->store($request);

            if ($newRow['success'] == true) {
                $response            = $this->generateResponseWithData('create-success', new CarResource($newRow['data']));
                $response['message'] = $request->get('name') . " successfully added.";

                return response()->json($response);
            }


            return response()->json($this->generateResponse($newRow['error']));
        }

        $errors = $validator->errors()->first();

        return response()->json($this->generateResponseWithError('validation-error', $errors));
    }

    public function update($id, Request $request)
    {
        $user = Auth::user();
        $car  = $user->cars()->find($id);

        if ($car) {
            // TODO: validate passed ids
            $validator = Validator::make($request->all(), [
                'name'          => 'bail|required|string',
                'plate_number'  => 'bail|required|string|max:5',
                'car_make_id'   => 'bail|required|integer',
                'car_model_id'  => 'bail|required|integer',
                'car_color_id'  => 'bail|required|integer',
                'car_type_id'   => 'bail|required|integer',
                'plate_type_id' => 'bail|required|integer',
                'plate_code_id' => 'bail|nullable|integer',
            ]);

            if ($validator->passes()) {
                $row = $this->cars->update($car, $request);

                if ($row['success'] == true) {
                    $response            = $this->generateResponseWithData('update-success', new CarResource($row['data']));
                    $response['message'] = $request->get('name') . " successfully updated.";

                    return response()->json($response);
                }

                return response()->json($this->generateResponse($row['error']));
            }

            $errors = $validator->errors()->first();

            return response()->json($this->generateResponseWithError('validation-error', $errors));
        }

        return response()->json($this->generateResponse('car-404'));
    }

    public function delete($id, Request $request)
    {
        $user = Auth::user();
        $car  = $user->cars()->find($id);

        if ($car) {
            $response            = $this->deleteById($id);
            $response['message'] = "Car successfully deleted.";

            return response()->json($response);
        }

        return response()->json($this->generateResponse('car-404'));
    }

    public function options()
    {
        $makes       = CarMake::all();
        $types       = CarType::all();
        $colors      = CarColor::all();
        $plate_types = PlateType::all();
        $plate_codes = PlateCode::all();

        $data = [
            'makes'       => CarMakeResource::collection($makes),
            'types'       => CarTypeResource::collection($types),
            'colors'      => CarColorResource::collection($colors),
            'plate_types' => PlateTypeResource::collection($plate_types),
            'plate_codes' => PlateCodeResource::collection($plate_codes),
        ];

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function makes()
    {
        $makes = CarMake::with('models')->get();
        $data  = CarMakeResource::collection($makes);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function models(Request $request)
    {
        $models = CarModel::query();

        if ($request->get('make_id')) {
            $models->where('car_make_id', $request->get('make_id'));
        }

        $models = $models->get();

        $data = CarModelResource::collection($models);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function types()
    {
        $types = CarType::all();
        $data  = CarTypeResource::collection($types);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function colors()
    {
        $colors = CarColor::all();
        $data   = CarColorResource::collection($colors);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function plate_types()
    {
        $codes = PlateType::all();
        $data  = PlateTypeResource::collection($codes);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function plate_codes()
    {
        $codes = PlateCode::all();
        $data  = PlateCodeResource::collection($codes);

        return response()->json($this->generateResponseWithData('success', $data));
    }
}
