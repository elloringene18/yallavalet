<?php

namespace App\Http\Controllers\Api;

use App\Models\Users\User;
use App\Models\Users\UserCar;
use App\Services\Traits\UserTraits;
use Illuminate\Http\Request;
use App\Models\Cars\Car;

use App\Services\CarServices;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        $data = $this->model->withCars()->get();

        foreach ($data as $user){
            $user->load('cars');
            foreach ($user->cars as $car)
                $car->load('color','type','plateType','model','make');
        }

        return $data;
    }
}
