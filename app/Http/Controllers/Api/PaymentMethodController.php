<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Models\PaymentMethods\PaymentMethod;
use App\Services\Traits\UserTraits;
use Illuminate\Http\Request;

class PaymentMethodController extends ApiController
{
    use UserTraits;

    public function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }

    public function fromUser($user_id)
    {
        return $this->model->getFromUser($user_id)->get();
    }
}
