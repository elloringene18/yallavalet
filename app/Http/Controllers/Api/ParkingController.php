<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\ParkedCarRequestResource;
use App\Http\Resources\ParkedCarResource;
use App\Models\Parking\ParkedCar;
use App\Models\Parking\ParkedCarRequest;
use App\Models\Parking\ParkingSpot;
use App\Services\CanCRUD;
use App\Services\ParkingServices;
use App\Services\PusherServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParkingController extends ApiController
{
    use CanCRUD;

    public function __construct(ParkingSpot $model, ParkingServices $parkingServices, ParkedCar $parkedCar, PusherServices $pusher)
    {
        $this->model      = $model;
        $this->cars       = $parkingServices;
        $this->parkedCars = $parkedCar;
        $this->pusher     = $pusher;
    }

    public function valetDashboard(Request $request)
    {
        $branch_id = $request->input('branch_id');

        $data = [
            'available_spaces' => $this->cars->getAvailableFromBranch($branch_id)['available']->count(),
            'parked'           => ParkedCarResource::collection($this->cars->getParkedFromBranch($branch_id)),
            'requests'         => ParkedCarRequestResource::collection($this->cars->getParkedCarRequestsFromBranch($branch_id))
        ];

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function getParkedForUser()
    {
        $user = Auth::user();
        $data = $this->cars->getParkedFromUser($user->id);

        return response()->json($this->generateResponseWithData('success', $data));
    }

    public function getParkingForBranch(Request $request)
    {
        $branch_id = $request->input('branch_id');

        return $this->generateResponseWithData('success', $this->cars->getAvailableFromBranch($branch_id));
    }

    public function getParkingForEstablishment($id)
    {
        return $this->model->getFromEstablishment($id)->get();
    }

    public function storeForUser(Request $request)
    {
        $newRow = $this->cars->store($request);

        if ($newRow != false)
            return response()->json($this->generateResponseWithData('create-success', $newRow));

        return response()->json($this->generateResponse('error'));
    }

    public function storeForBranch(Request $request)
    {
        $newRow = $this->cars->store($request);

        if ($newRow != false)
            return response()->json($this->generateResponseWithData('create-success', $newRow));

        return response()->json($this->generateResponse('error'));
    }

    public function delete(Request $request, $id)
    {
        return response()->json($this->deleteById($id));
    }

    public function parkACar(Request $request)
    {
        $newRow = $this->cars->park($request);

        if ($newRow['success']){
            $this->pusher->push('channel-1','car-parked');
            return response()->json($this->generateResponseWithData('create-success', $newRow));
        }

        return response()->json($this->generateResponse($newRow['error']));
    }

    public function requestACar(Request $request)
    {
        $newRow = $this->cars->requestACar($request);

        if ($newRow['success']) {

            $this->pusher->push('channel-1','car-return-requested');
            $data = new ParkedCarRequestResource($newRow['data']);

            return response()->json($this->generateResponseWithData('create-success', $data));
        }

        return response()->json($this->generateResponse($newRow['error']));
    }

    public function carRequestAccepted(Request $request)
    {
        $result = $this->cars->carRequestAccepted($request);

        if ($result['success']) {
            $this->pusher->push('channel-1','car-request-accepted');
            $data = new ParkedCarRequestResource($result['data']);

            return response()->json($this->generateResponseWithData('success', $data));
        }

        return response()->json($this->generateResponse($result['error']));
    }

    public function carRequestCancelled(Request $request) {
        $result = $this->cars->carRequestCancelled($request);

        if ($result['success']) {
            $this->pusher->push('channel-1','car-return-cancelled');
            $data = new ParkedCarRequestResource($result['data']);

            return response()->json($this->generateResponseWithData('success', $data));
        }

        return response()->json($this->generateResponse($result['error']));
    }

    public function carArrived(Request $request) {
        $result = $this->cars->carArrived($request);

        if ($result['success']) {
            $this->pusher->push('channel-1','car-arrived');
            $data = new ParkedCarRequestResource($result['data']);

            return response()->json($this->generateResponseWithData('success', $data));
        }

        return response()->json($this->generateResponse($result['error']));
    }

    public function carReturned(Request $request)
    {
        $result = $this->cars->carReturned($request);

        if ($result['success']) {
            $this->pusher->push('channel-1','car-returned');
            $data = new ParkedCarRequestResource($result['data']);

            return response()->json($this->generateResponseWithData('success', $data));
        }

        return response()->json($this->generateResponse($result['error']));
    }

    public function getParkedCars(Request $request)
    {
        $data = $this->cars->getParkedFromBranch($request->input('branch_id'));

        if (count($data))
            return response()->json($this->generateResponseWithData('record-found', $data));

        return response()->json($this->generateResponse('no-record-found'));
    }

    public function getParkedCarsForUser(Request $request)
    {
        $data = $this->cars->getParkedCarsForUser($request->input('user_id'));

        if(count($data))
            return response()->json($this->generateResponseWithData('record-found',$data));

        return response()->json($this->generateResponse('no-record-found'));
    }

    public function getParkedCarRequestsFromBranch(Request $request)
    {
        $data = $this->cars->getParkedCarRequestsFromBranch($request->input('branch_id'));

        if (count($data))
            return response()->json($this->generateResponseWithData('record-found', $data));

        return response()->json($this->generateResponse('no-record-found'));
    }

    public function getActiveParkedCars(Request $request)
    {
        $data = $this->parkedCars->where('left_on', '==', 'null');

        if (count($data))
            return response()->json($this->generateResponseWithData('record-found', $data));

        return response()->json($this->generateResponse('no-record-found'));
    }


}
