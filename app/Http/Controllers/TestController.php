<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars\Car;
use Pusher\Pusher as Pusher;
use App\Services\PusherServices;

class TestController extends Controller
{
	
	public function __construct(PusherServices $pusher)
	{
	    $this->pusher = $pusher;
	}
	
    public function QrFromCar($id){
		$car = Car::find($id);
		return car;
	}

	public function sendPush(){

		  $data['message'] = 'hello world';
		  $this->pusher->push('channel-1','my-event',$data);

		  return view('welcome');
	}
}
