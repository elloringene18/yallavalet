<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ParkingSpotResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'code'   => $this->code,
            'price'  => $this->price,
            'branch' => new BranchResource($this->whenLoaded('branch'))
        ];
    }
}
