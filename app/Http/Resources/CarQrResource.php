<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CarQrResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'base64' => $this->base64_code
        ];
    }
}
