<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ParkedCarResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'parking_spot' => new ParkingSpotResource($this->whenLoaded('parking_spot')),
            'car'          => new CarResource($this->whenLoaded('car')),
            'request'      => new ParkedCarRequestResource($this->whenLoaded('request')),
            'returned_on'  => (string)$this->returned_on,
            'created_at'   => (string)$this->created_at,
            'updated_at'   => (string)$this->updated_at,
        ];
    }
}
