<?php

namespace App\Http\Resources;

use App\Models\Cars\CarModel;
use Illuminate\Http\Resources\Json\Resource;

class CarMakeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name,
            'models' => CarModelResource::collection($this->models)
        ];
    }
}
