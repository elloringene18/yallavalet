<?php

namespace App\Http\Resources;

use App\Models\Users\User;
use App\Services\CanJWT;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class UserResource extends Resource
{
    protected $token;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id'         => $this->id,
            'name'       => $this->name,
            'email'      => $this->email,
            'role'       => new RoleResource($this->whenLoaded('role')),
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at
        ];

        // Add Branch
        if ($this->role->slug == 'valet') {
            $data['branch'] = new BranchResource($this->branch->load('establishment', 'establishment.type')->first());
        }

        if ($this->token) {
            $data['token'] = $this->token;
        }

        return $data;
    }

    public function addToken($token)
    {
        $this->token = $token;
    }
}
