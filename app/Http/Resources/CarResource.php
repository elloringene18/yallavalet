<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CarResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                     => $this->id,
            'name'                   => $this->name,
            'make'                   => new CarMakeResource($this->make),
            'type'                   => new CarTypeResource($this->type),
            'model'                  => new CarModelResource($this->model),
            'color'                  => new CarColorResource($this->color),
            'plate_type'             => new PlateTypeResource($this->plateType),
            'plate_code'             => new PlateCodeResource($this->plateCode->first()),
            'plate_number'           => $this->plate_number,
            'formatted_plate_number' => $this->formatted_plate_number,
            'qr_code'                => new CarQrResource($this->qrCode),
            'users'                  => UserResource::collection($this->whenLoaded('users')),
            'created_at'             => (string)$this->created_at,
            'updated_at'             => (string)$this->updated_at
        ];
    }
}
