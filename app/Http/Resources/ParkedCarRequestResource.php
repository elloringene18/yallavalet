<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ParkedCarRequestResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'parked_car' => new ParkedCarResource($this->whenLoaded('parked_car')),
            'driver'     => new UserResource($this->whenLoaded('driver')),
            'status'     => (integer) $this->status,
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
