<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkedCarPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parked_car_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parked_car_id')->unsigned();
            $table->foreign('parked_car_id')->references('id')->on('parked_cars')->onDelete('cascade');
            $table->float('amount');
            $table->string('payment_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parked_car_payments');
    }
}
