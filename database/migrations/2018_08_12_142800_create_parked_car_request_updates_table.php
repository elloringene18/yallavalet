<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkedCarRequestUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parked_car_request_updates', function (Blueprint $table) {
            //'status','parked_car_request_id','driver_id'
            $table->increments('id');
            $table->integer('parked_car_request_id')->unsigned()->indexed();
            $table->foreign('parked_car_request_id')->references('id')->on('parked_car_requests')->onDelete('cascade');
            $table->integer('parked_car_request_type_id')->unsigned()->indexed();
            $table->foreign('parked_car_request_type_id')->references('id')->on('parked_car_request_types')->onDelete('cascade');
            $table->integer('driver_id')->nullable();
            $table->foreign('driver_id')->references('id')->on('branch_drivers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parked_car_request_updates');
    }
}
