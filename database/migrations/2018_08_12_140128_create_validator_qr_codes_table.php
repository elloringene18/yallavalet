<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidatorQrCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validator_qr_codes', function (Blueprint $table) {
            $table->text('base64_code');
            $table->integer('validator_id')->unsigned()->indexed();
            $table->foreign('validator_id')->references('id')->on('validators')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validator_qr_codes');
    }
}
