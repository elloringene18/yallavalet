<?php

use Illuminate\Database\Seeder;

class CarColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Red','code' => '#ff0000'],
            ['name' => 'Blue','code' => '#0000ff'],
            ['name' => 'White','code' => '#ffffff'],
            ['name' => 'Black','code' => '#000000'],
        ];

        foreach ($items as $item) {
            \App\Models\Cars\CarColor::create($item);
        }
    }
}
