<?php

use Illuminate\Database\Seeder;
use App\Models\Users\User;
use App\Models\Users\Role;
use App\Models\Cars\Car;
use App\Models\Cars\PlateType;
use App\Models\Cars\CarType;
use App\Models\Cars\CarColor;
use App\Models\Cars\CarMake;
use App\Models\Cars\CarModel;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{

    public function __construct(\Faker\Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Eloquent::unguard();

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        User::truncate();

        // supposed to only apply to a single connection and reset it's self
        // but I like to explicitly undo what I've done for clarity
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $users = [];
        $usersToCreate = 30;

        for($x=1;$x<$usersToCreate;$x++){
            $users[] =
            [
                'name' => $this->faker->company, 'email' => $this->faker->email, 'password' => \Illuminate\Support\Facades\Hash::make('secret')
            ];
        }

        foreach ($users as $user)
        {
            $user = User::create($user);

            if($user!=null)
            {
                $cars = $this->getRandomCarData();
                $paymentMethods = $this->getRandomPaymentMethodData();

				$adminRole = Role::where('slug','admin')->first();
				
                $user->cars()->createMany($cars);
                $user->role()->sync($adminRole->id);
                $user->paymentMethods()->createMany($paymentMethods);
            }
        }
    }

    public function getRandomCarData(){

        $randomPlateNumber = PlateType::inRandomOrder()->first();
        $randomCarType = CarType::inRandomOrder()->first();
        $randomMake = CarMake::inRandomOrder()->first();
        $randomModel = CarModel::inRandomOrder()->first();
        $randomColor = CarColor::inRandomOrder()->first();

        $carCount = rand(1,5);
        $data = [];

        for($x=1;$x<=$carCount;$x++){
            $data[$x] =
                [
                    'plate_number' => \Illuminate\Support\Str::random('6'),
                    'plate_type_id' => $randomPlateNumber ? $randomPlateNumber->id : 1,
                    'car_type_id' => $randomCarType ? $randomCarType->id : 1,
                    'car_make_id' => $randomMake ? $randomMake->id : 1,
                    'car_model_id' => $randomModel ? $randomModel->id : 1,
                    'car_color_id' => $randomColor ? $randomColor->id : 1,
                ];
        }

        return $data;
    }

    public function getRandomPaymentMethodData(){

        $count = rand(1,5);
        $data = [];

        for($x=1;$x<=$count;$x++){
            $data[$x] =
                [
                    'card_type' => $this->faker->creditCardType,
                    'card_number' => $this->faker->creditCardNumber,
                    'card_expire' => $this->faker->creditCardExpirationDate,
                    'card_cvv' => Str::random('3'),
                    'card_label' => Str::random('6'),
                ];
        }

        return $data;
    }
}
