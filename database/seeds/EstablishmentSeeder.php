<?php

use Illuminate\Database\Seeder;
use App\Models\Establishments\EstablishmentType;
use App\Models\Establishments\Establishment;

class EstablishmentSeeder extends Seeder
{

    public function __construct(\Faker\Generator $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Eloquent::unguard();

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        EstablishmentType::truncate();
        Establishment::truncate();

        // supposed to only apply to a single connection and reset it's self
        // but I like to explicitly undo what I've done for clarity
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $types = [];

        for($x=0;$x<20;$x++){
            $types[] = EstablishmentType::create(['name'=>$this->faker->jobTitle]);
        }

//        for($x=0;$x<20;$x++){
//            $establishment = Establishment::create([
//                'name' => $this->faker->company,
//                'establishment_type_id' => $types[$x]->id
//            ]);
//
//            if($establishment!=null){
//                $establishment->settings()->createMany([
//                   [
//                       'key' => 'what',
//                       'value' => 'this'
//                   ],
//                   [
//                       'key' => 'when',
//                       'value' => 'now'
//                   ],
//                   [
//                       'key' => 'who',
//                       'value' => 'me'
//                   ],
//                ]);
//                $establishment->branches()->createMany([
//                   [
//                       'name' => 'Dubai Branch',
//                   ],
//                   [
//                       'name' => 'Sharjah Branch',
//                   ],
//                   [
//                       'name' => 'Al Ain Branch',
//                   ],
//                ]);
//            }
//        }
    }
}
