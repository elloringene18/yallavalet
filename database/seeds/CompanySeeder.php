<?php

use Illuminate\Database\Seeder;
use App\Models\Establishments\Establishment;
use App\Models\Users\User;
use App\Models\Users\Role;

class CompanySeeder extends Seeder
{

    public function __construct(\Faker\Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Eloquent::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Establishment::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // CREATE ESTABLISHMENTS

        $establishments = [];
        $establishmentsToCreate = 5;

        for($x=1;$x<$establishmentsToCreate;$x++){
            $establishments[] =
                [
                    'name' => $this->faker->company, 'establishment_type_id' => rand(1,20)
                ];
        }

        foreach ($establishments as $establishment)
        {
            $newEstablishment = Establishment::create($establishment);

            if($newEstablishment!=null)
            {
                // ADD ESTABLISHMENT BRANCHES

                $branchesToCreate = 2;

                for($x=1;$x<$branchesToCreate;$x++)
                    $newEstablishment->branches()->create(['name' => $this->faker->country,'pusher_channel'=>\Illuminate\Support\Str::random('16')]);

                $newBranches = $newEstablishment->branches()->get();

                foreach($newBranches as $newBranch){

                    // CREATE BRANCH PARKING SPOTS

                    $parkingSpotsToCreate = 20;

                    for($x=1;$x<$parkingSpotsToCreate;$x++)
                        $newBranch->parkingSpots()->create([
                            'code' => chr(rand(65,90)).rand(0,99),
                            'price' => rand(0,99)
                        ]);

                    // CREATE BRANCH DRIVERS

                    $branchDriversToCreate = 3;
                    $valetRole = Role::where('slug','valet')->first();

                    for($x=1;$x<$branchDriversToCreate;$x++){
                        $user = $newBranch->drivers()->create([
                            'name' => $this->faker->name, 'email' => $this->faker->email, 'password' => \Illuminate\Support\Facades\Hash::make('secret')
                        ]);
                        $user->role()->sync($valetRole->id);
                    }

                }


            }
        }


    }
}
