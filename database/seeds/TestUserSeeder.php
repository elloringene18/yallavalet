<?php

use App\Models\Establishments\Branch;
use App\Models\Users\Role;
use App\Models\Users\User;
use Illuminate\Database\Seeder;

class TestUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test_users = [
            [
                'name'      => 'Jeffrey Bello',
                'email'     => 'yerffej19@gmail.com',
                'password'  => bcrypt('jeffrey'),
                'role_slug' => 'user'
            ],
            [
                'name'      => 'Test User',
                'email'     => 'user@test.com',
                'password'  => bcrypt('test'),
                'role_slug' => 'user'
            ],
            [
                'name'      => 'Test Valet',
                'email'     => 'valet@test.com',
                'password'  => bcrypt('valet'),
                'role_slug' => 'valet'
            ]
        ];

        foreach ($test_users as $test_user) {
            $is_exist = User::where('email', $test_user['email'])->first();

            if (!$is_exist) {
                /** @var User $user */
                $user           = new User();
                $user->name     = $test_user['name'];
                $user->email    = $test_user['email'];
                $user->password = $test_user['password'];
                $user->save();

                $role = Role::where('slug', $test_user['role_slug'])->first();
                $user->role()->sync($role->id);

                if ($test_user['role_slug'] == 'valet') {
                    $branch = Branch::find(1);
                    $user->branch()->sync($branch->id);
                }
            }
        }
    }
}
