<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'     => 'United Arab Emirates',
                'code'    => 'AE',
            ]
        ];

        foreach ($data as $datum) {
            $is_exist = Country::where([
					['name', $datum['name']],
					['code', $datum['code']]
				])->first();

            if (!$is_exist) {
                Country::create($datum);
            }
        }
    }
}
