<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(CountrySeeder::class);
         $this->call(PlateCodeSeeder::class);
         $this->call(CarMakeSeeder::class);
         $this->call(CarModelSeeder::class);
         $this->call(CarColorSeeder::class);
         $this->call(CarTypeSeeder::class);
         $this->call(PlateTypeSeeder::class);
         $this->call(CarSeeder::class);
         $this->call(EstablishmentSeeder::class);
         $this->call(CompanySeeder::class);
         $this->call(TestUserSeeder::class);
    }
}
