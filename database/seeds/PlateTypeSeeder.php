<?php

use Illuminate\Database\Seeder;

class PlateTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Government'],
            ['name' => 'Private'],
            ['name' => 'Public']
        ];

        foreach ($items as $item) {
            \App\Models\Cars\PlateType::create($item);
        }
    }
}
