<?php

use Illuminate\Database\Seeder;

class CarMakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'name' => 'ABARTH',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]

            ],
            ['name' => 'ALFA ROMEO',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]

            ],

            ['name' => 'ASTON MARTIN',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]

            ],
            ['name' => 'AUSTIN',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'BEDFORD',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'BENTLEY',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'BOLWELL',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'BUFORI',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CADILLAC',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CATERHAM',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CHERY',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CHEVROLET',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CHRYSLER',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'CITROEN',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'DAEWOO',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'DAIHATSU',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'DODGE',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'FIAT',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'GEELY',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'GREAT WALL',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'HINO',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'HUMMER',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'INFINITI',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'ISUZU',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'JAGUAR',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'JEEP',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'LAND ROVER',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'LEXUS',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'LOTUS',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'MINI',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'MITSUBISHI',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'OPEL',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'PORSCHE',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'PROTON',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'RANGE ROVER',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'RENAULT',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'SAAB',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'SKODA',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'SSANGYONG',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'SUBARU',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'SUZUKI',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'TATA',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
            ['name' => 'VOLVO',
                'models' => [
                    [ 'name' => 'Model 1' ],
                    [ 'name' => 'Model 2' ],
                    [ 'name' => 'Model 3' ],
                    [ 'name' => 'Model 4' ],
                    [ 'name' => 'Model 5' ],
                ]
            ],
        ];

        foreach ($items as $item) {
            $make = \App\Models\Cars\CarMake::create(['name'=>$item['name']]);

            if($make!=null){
                foreach ($item['models'] as $model) {
                    $make->models()->create([
                        'name'=>$item['name'].' '.$model['name']
                    ]);
                }
            }

        }
    }
}
