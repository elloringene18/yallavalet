<?php

use Illuminate\Database\Seeder;
use App\Models\Cars\PlateCode;
use App\Models\Country;

class PlateCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		$targetCountry = Country::where('code','AE')->first();
		
		if($targetCountry==null)
			return false;
			
		for($x=1;$x<20;$x++)
			$data[] = $x;
			
        $letters = range('A', 'Z');
		foreach($letters as $letter)
			$data[] = $letter;
		

        foreach ($data as $datum)
			$targetCountry->plateCode()->create([
				'code'=> $datum
			]);
    }
}
