<?php

use Illuminate\Database\Seeder;

class CarTypeSeeder extends Seeder
{


    public function __construct(\Faker\Generator $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Sedan'],
            ['name' => 'Coupe'],
            ['name' => 'SUV'],
            ['name' => 'Van'],
        ];

        foreach ($items as $item) {
            \App\Models\Cars\CarType::create($item);
        }
    }
}
