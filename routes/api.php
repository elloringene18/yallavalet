<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });

    Route::group(['prefix' => 'user', 'as' => 'user.', 'middleware' => 'auth'], function() {

        Route::group(['prefix' => 'cars'], function() {
            Route::get('/', ['uses' => 'Api\CarController@fromCurrentUser', 'as' => 'cars']);
            Route::get('/parked', ['uses' => 'Api\ParkingController@getParkedForUser', 'as' => 'cars.parked']);
        });
    });

    Route::group(['prefix' => 'login'], function () {
        Route::post('/', ['uses' => 'Api\LoginController@login', 'as' => 'login']);
        Route::post('/facebook', ['uses' => 'Api\LoginController@facebook', 'as' => 'login.facebook']);
        Route::post('/google', ['uses' => 'Api\LoginController@google', 'as' => 'login.google']);
    });

    Route::post('register', ['uses' => 'Api\LoginController@register', 'as' => 'register']);

    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        // Get user with all data such as cars and payment methods etc. ( No payment method on production )
        Route::get('/', ['uses' => 'Api\UserController@all', 'as' => 'all']);
        Route::get('/from-user/{id}', ['uses' => 'Api\UserController@fromUser', 'as' => 'fromUser']);
        Route::post('/from-current-user', ['uses' => 'Api\UserController@fromCurrentUser', 'as' => 'fromCurrentUser']);
    });
	
    Route::group(['prefix' => 'test', 'as' => 'test.'], function () {
        // Get user with all data such as cars and payment methods etc. ( No payment method on production )
        Route::get('/qr-from-car/{id}', ['uses' => 'TestController@QrFromCar', 'as' => 'QrFromCar']);
    });

    Route::group(['prefix' => 'cars', 'as' => 'cars.'], function () {
        Route::get('/', ['uses' => 'Api\CarController@all', 'as' => 'all']);
        Route::get('/from-user/{id}', ['uses' => 'Api\CarController@fromUser', 'as' => 'from-user']);
        Route::post('/from-current-user', ['uses' => 'Api\CarController@fromCurrentUser', 'as' => 'from-current-user', 'middleware' => 'auth']);
        Route::post('/store', ['uses' => 'Api\CarController@store', 'as' => 'store', 'middleware' => 'auth']);
        Route::put('/update/{id}', ['uses' => 'Api\CarController@update', 'as' => 'update', 'middleware' => 'auth']);
        Route::delete('/delete/{id}', ['uses' => 'Api\CarController@delete', 'as' => 'delete', 'middleware' => 'auth']);

        // Options
        Route::get('/makes', ['uses' => 'Api\CarController@makes', 'as' => 'makes']);
        Route::get('/models', ['uses' => 'Api\CarController@models', 'as' => 'models']);
        Route::get('/types', ['uses' => 'Api\CarController@types', 'as' => 'types']);
        Route::get('/colors', ['uses' => 'Api\CarController@colors', 'as' => 'colors']);
        Route::get('/plate-types', ['uses' => 'Api\CarController@plate_types', 'as' => 'plate-types']);
        Route::get('/plate-codes', ['uses' => 'Api\CarController@plate_codes', 'as' => 'plate-codes']);
        Route::get('/options', ['uses' => 'Api\CarController@options', 'as' => 'options']);

        // QR Scan API
        Route::get('/scan/{id}', ['uses' => 'Api\QRCodeController@findCarById', 'scan']);
    });

    Route::group(['prefix' => 'establishments', 'as' => 'establishments.'], function () {
        Route::get('/', ['uses' => 'Api\EstablishmentController@all', 'as' => 'all']);
        Route::post('/store', ['uses' => 'Api\EstablishmentController@store', 'as' => 'store']);
        Route::delete('/delete/{id}', ['uses' => 'Api\EstablishmentController@delete', 'as' => 'delete']);

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::post('/store', ['uses' => 'Api\EstablishmentController@addSettings', 'as' => 'store']);
            Route::get('/get/{id}', ['uses' => 'Api\EstablishmentController@getSettings', 'as' => 'get']);
            Route::delete('/delete/{id}', ['uses' => 'Api\EstablishmentController@deleteSettings', 'as' => 'get']);
        });

        Route::group(['prefix' => 'types', 'as' => 'types.'], function () {
            Route::get('/', ['uses' => 'Api\EstablishmentController@all', 'as' => 'all']);
            Route::post('/store', ['uses' => 'Api\EstablishmentController@store', 'as' => 'store']);
        });
    });

    Route::group(['prefix' => 'parking', 'as' => 'parking.'], function () {
        Route::get('/', ['uses' => 'Api\ParkingController@all', 'as' => 'all']);
        Route::post('/park', ['uses' => 'Api\ParkingController@parkACar', 'as' => 'store']);
        Route::post('/request', ['uses' => 'Api\ParkingController@requestACar', 'as' => 'park']);
        Route::post('/accepted', ['uses' => 'Api\ParkingController@carRequestAccepted', 'as' => 'accepted']);
        Route::post('/cancelled', ['uses' => 'Api\ParkingController@carRequestCancelled', 'as' => 'cancelled']);
        Route::post('/arrived', ['uses' => 'Api\ParkingController@carArrived', 'as' => 'arrived']);
        Route::post('/returned', ['uses' => 'Api\ParkingController@carReturned', 'as' => 'returned']);
        Route::get('/getParked/branch/{id}', ['uses' => 'Api\ParkingController@getParkedCars', 'as' => 'getParked']);

        Route::group(['prefix' => 'branches', 'as' => 'branches.'], function () {
            Route::get('/', ['uses' => 'Api\BranchController@all', 'as' => 'all']);
            Route::post('/store', ['uses' => 'Api\BranchController@store', 'as' => 'store']);
            Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@delete', 'as' => 'delete']);

            Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
                Route::post('/store', ['uses' => 'Api\BranchController@addSettings', 'as' => 'store']);
                Route::get('/get/{id}', ['uses' => 'Api\BranchController@getSettings', 'as' => 'get']);
                Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@deleteSettings', 'as' => 'get']);
            });

            Route::group(['prefix' => 'parking', 'as' => 'parking.'], function () {
                Route::post('/store', ['uses' => 'Api\BranchController@addParking', 'as' => 'store']);
                Route::get('/get/user/{id}', ['uses' => 'Api\BranchController@getParkingForUser', 'as' => 'get']);
                Route::get('/get/branch/{id}', ['uses' => 'Api\BranchController@getParking', 'as' => 'get']);
                Route::get('/get/establishment/{id}', ['uses' => 'Api\BranchController@getParkingForEstablishment', 'as' => 'get']);
                Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@deleteParking', 'as' => 'get']);


                Route::post('/get-parking-spaces', ['uses' => 'Api\ParkingController@getParkingForBranch', 'as' => 'getParkingSpaces']);
                Route::post('/get-parked-cars', ['uses' => 'Api\ParkingController@getParkedCars', 'as' => 'getParkedCars']);
                Route::post('/get-parked-cars-for-user', ['uses' => 'Api\ParkingController@getParkedCarsForUser', 'as' => 'getParkedCarsForUser']);
                Route::post('/get-parked-car-requests', ['uses' => 'Api\ParkingController@getParkedCarRequestsFromBranch', 'as' => 'getParkedCarRequestsFromBranch']);
            });
        });

        Route::group(['prefix' => 'payment-methods', 'as' => 'cars.'], function () {
            Route::get('/', ['uses' => 'Api\CarController@all', 'as' => 'all']);
            Route::get('/from-user/{user_id}', ['uses' => 'Api\PaymentMethodController@fromUser', 'as' => 'from-user']);
        });

    });

    Route::group(['prefix' => 'branches', 'as' => 'branches.'], function () {
        Route::get('/', ['uses' => 'Api\BranchController@all', 'as' => 'all']);
        Route::post('/store', ['uses' => 'Api\BranchController@store', 'as' => 'store']);
        Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@delete', 'as' => 'delete']);

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::post('/store', ['uses' => 'Api\BranchController@addSettings', 'as' => 'store']);
            Route::get('/get/{id}', ['uses' => 'Api\BranchController@getSettings', 'as' => 'get']);
            Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@deleteSettings', 'as' => 'get']);
        });

        Route::group(['prefix' => 'parking', 'as' => 'parking.'], function () {
            Route::post('/store', ['uses' => 'Api\BranchController@addParking', 'as' => 'store']);
            Route::get('/get/user/{id}', ['uses' => 'Api\BranchController@getParkingForUser', 'as' => 'get']);
            Route::get('/get/branch/{id}', ['uses' => 'Api\BranchController@getParking', 'as' => 'get']);
            Route::get('/get/establishment/{id}', ['uses' => 'Api\BranchController@getParkingForEstablishment', 'as' => 'get']);
            Route::delete('/delete/{id}', ['uses' => 'Api\BranchController@deleteParking', 'as' => 'get']);


            Route::post('/dashboard', ['uses' => 'Api\ParkingController@valetDashboard', 'as' => 'dashboard']);
            Route::post('/get-parking-spaces', ['uses' => 'Api\ParkingController@getParkingForBranch', 'as' => 'getParkingSpaces'])->middleware('auth');
            Route::post('/get-parked-cars', ['uses' => 'Api\ParkingController@getParkedCars', 'as' => 'getParkedCars']);
            Route::post('/get-parked-car-requests', ['uses' => 'Api\ParkingController@getParkedCarRequestsFromBranch', 'as' => 'getParkedCarRequestsFromBranch']);
        });
    });
});